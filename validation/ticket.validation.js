const { body } = require("express-validator");
const { Customer, Event } = require("../models");

module.exports = {
  create: () => [
    body("customer_id").custom((value) => {
      return Customer.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Customer with id ${value} is not found`);
      });
    }),
    body("event_id").custom((value) => {
      return Event.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Event with id ${value} is not found`);
      });
    }),
  ],
  update: () => [
    body("customer_id").custom((value) => {
      return Customer.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Customer with id ${value} is not found`);
      });
    }),
    body("event_id").custom((value) => {
      return Event.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Event with id ${value} is not found`);
      });
    }),
  ],
};
