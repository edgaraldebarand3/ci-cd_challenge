const {
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  GOOGLE_REFRESH_TOKEN,
  EMAIL_BUSINESS,
} = process.env;
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET);

client.setCredentials({
  refresh_token: GOOGLE_REFRESH_TOKEN,
});

const sendEmail = (from, receiver, subject, messageToBeSent) => {
  const accessToken = client.getAccessToken();

  const mailOptions = {
    from: `${from}`,
    to: `${receiver}`,
    subject: `${subject}`,
    html: `${messageToBeSent}`,
  };

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: EMAIL_BUSINESS,
      clientId: client._clientId,
      clientSecret: client._clientSecret,
      refreshToken: GOOGLE_REFRESH_TOKEN,
      accessToken: accessToken,
    },
  });

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) reject(err);
      else resolve(info);
    });
  });
};

module.exports = sendEmail;
