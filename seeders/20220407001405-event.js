"use strict";

const eventData = require("../masterdata/event.json");

module.exports = {
  async up(queryInterface, Sequelize) {
    const eventDataWithoutId = eventData.map((eachEventData) => {
      delete eachEventData.id;

      eachEventData.createdAt = new Date();
      eachEventData.updatedAt = new Date();
      return eachEventData;
    });
    await queryInterface.bulkInsert("Events", eventDataWithoutId);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Events", null);
  },
};
