"use strict";

const ticketData = require("../masterdata/ticket.json");

module.exports = {
  async up(queryInterface, Sequelize) {
    const ticketDataWithoutId = ticketData.map((eachTicketData) => {
      delete eachTicketData.id;

      eachTicketData.createdAt = new Date();
      eachTicketData.updatedAt = new Date();
      return eachTicketData;
    });
    await queryInterface.bulkInsert("Tickets", ticketDataWithoutId);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Tickets", null);
  },
};
