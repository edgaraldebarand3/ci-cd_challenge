const router = require("express").Router();
const userController = require("../controllers/users.controller");
const validate = require("../validation/validate");
const userValidator = require("../validation/user.validation");
const uploadImage = require("../misc/multerImage");
const uploadVideo = require("../misc/multerVideo");
const {
  uploadWithCloudinary,
  uploadVideoWithCloudinary,
} = require("../misc/cloudinary");
const {
  verifyToken,
  verifyTokenAndMember,
  verifyTokenAndAdmin,
  verifyTokenAndSuperAdmin,
  verifyTokenAdminAndSA,
} = require("../misc/verifyRole");

router.get("/", verifyToken, userController.getAll);
router.post("/login", userController.login);
router.post("/register", userController.register);
router.post(
  "/create",
  // verifyTokenAdminAndSA,
  uploadImage.single("profile_picture"),
  userValidator.create(),
  validate,
  uploadWithCloudinary,
  userController.create
);
//video
router.post(
  "/upload-video",
  uploadVideo.single("video"),
  uploadVideoWithCloudinary,
  userController.uploadVideo
);
//Get OTP
router.post("/get-otp", userController.getOtp);

//Forget Password
router.post("/forget-password", userController.forgetPassword);

router.put("/:id", verifyTokenAndSuperAdmin, userController.update);
router.delete("/:id", verifyTokenAndSuperAdmin, userController.delete);

module.exports = router;
