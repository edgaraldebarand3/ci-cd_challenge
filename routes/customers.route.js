const express = require("express");
const router = express.Router();
const {
  getAllCustomer,
  getCustomerById,
  createCustomer,
  updateCustomerById,
  deleteCustomerById,
} = require("../controllers/customers.controller");
const { userValidation } = require("../misc/middleware");


router.get("/", userValidation, getAllCustomer);
router.get("/:id", userValidation, getCustomerById);
router.post("/", userValidation, createCustomer);
router.put("/:id", userValidation, updateCustomerById);
router.delete("/:id", userValidation, deleteCustomerById);

module.exports = router;
