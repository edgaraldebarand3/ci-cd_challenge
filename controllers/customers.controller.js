const { Customer } = require("../models");

const getAllCustomer = async (req, res) => {
  try {
    let { page, row } = req.query;

    page -= 1;

    const options = {
      attributes: ["id", "name", "age", "phone_number"],
      offset: page * row,
      limit: row,
      order: [["id", "ASC"]],
    };

    const allCustomer = await Customer.findAll(options);
    return res.status(200).json({
      status: "success",
      result: allCustomer,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const getCustomerById = async (req, res) => {
  try {
    const foundCustomer = await Customer.findByPk(req.params.id);
    if (!foundCustomer) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(200).json({
      status: "Success",
      data: foundCustomer,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const createCustomer = async (req, res) => {
  try {
    const { name, age, phone_number } = req.body;

    const createdCustomer = await Customer.create({
      name: name,
      age: age,
      phone_number: phone_number,
    });
    return res.status(201).json({
      status: "Success",
      data: createdCustomer,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const updateCustomerById = async (req, res) => {
  const customerId = req.params.id;
  try {
    const { name, age, phone_number } = req.body;
    const updateCustomer = await Customer.update(
      { name: name, age: age, phone_number: phone_number },
      {
        where: {
          id: customerId,
        },
      }
    );
    const foundCustomer = await Customer.findOne({
      where: { id: customerId },
    });
    if (!foundCustomer) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      data: req.body,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const deleteCustomerById = async (req, res) => {
  const customerId = req.params.id;
  try {
    const deleteCustomer = await Customer.destroy({
      where: {
        id: customerId,
      },
    });
    if (deleteCustomer == 0) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      msg: "Data berhasil di delete",
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

module.exports = {
  getAllCustomer,
  getCustomerById,
  createCustomer,
  updateCustomerById,
  deleteCustomerById,
};
