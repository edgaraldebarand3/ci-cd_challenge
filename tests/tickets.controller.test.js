const app = require("../app");

const { sequelize, Ticket } = require("../models");

const request = require("supertest");

const ticketData = require("../masterdata/ticket.json").map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  return eachData;
});
const customerData = require("../masterdata/customer.json").map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  return eachData;
});
const eventData = require("../masterdata/event.json").map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  return eachData;
});

beforeAll((done) => {
  sequelize.queryInterface.bulkInsert("Customers", customerData, {});
  sequelize.queryInterface.bulkInsert("Events", eventData, {});
  sequelize.queryInterface
    .bulkInsert("Tickets", ticketData, {})
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

afterAll((done) => {
  sequelize.queryInterface.bulkDelete("Customers", null, {
    truncate: true,
    restartIdentity: true,
  });
  sequelize.queryInterface.bulkDelete("Events", null, {
    truncate: true,
    restartIdentity: true,
  });
  sequelize.queryInterface
    .bulkDelete("Tickets", null, {
      truncate: true,
      restartIdentity: true,
    })
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

describe("ticketsController get function", () => {
  // POSITIVE TEST
  test("get all ticket data success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .get("/ticket?page=1&row=3")
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("success");
    expect(result.length).toBeGreaterThan(0);
  });

  test("get ticket by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app).get("/ticket/1").set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("get ticket by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app).get("/ticket/999").set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});

describe("ticketController put function", () => {
  // POSITIVE TEST
  test("update data ticket success", async () => {
    const createData = await Ticket.create({
      ticket_name: "A04",
      price: 55000,
      customer_id: 2,
      event_id: 1,
    });
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      ticket_name: "SYNC04",
      price: 55000,
      customer_id: 2,
      event_id: 1,
    };
    const response = await request(app)
      .put(`/ticket/${createData.id}`)
      .set(userValidation)
      .send(data);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  // NEGATIVE TEST
  test("update data ticket failed because id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      ticket_name: "SYNC04",
      price: 55000,
      customer_id: 2,
      event_id: 1,
    };
    const response = await request(app)
      .put(`/ticket/999`)
      .set(userValidation)
      .send(data);

    const { status, result } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
  });
});

describe("ticketController delete function", () => {
  // POSITIVE TEST

  test("delete ticket by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const createData = await Ticket.create({
      ticket_name: "A04",
      price: 55000,
      customer_id: 2,
      event_id: 1,
    });
    const response = await request(app)
      .delete(`/ticket/${createData.id}`)
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("delete ticket by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .delete("/ticket/999")
      .set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});
