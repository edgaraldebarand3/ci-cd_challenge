const app = require("../app");

const { sequelize, Customer } = require("../models");

const request = require("supertest");

const customerData = require("../masterdata/customer.json").map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  return eachData;
});

beforeAll((done) => {
  sequelize.queryInterface
    .bulkInsert("Customers", customerData, {})
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

afterAll((done) => {
  sequelize.queryInterface
    .bulkDelete("Customers", null, {
      truncate: true,
      restartIdentity: true,
    })
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

describe("customersController get function", () => {
  // POSITIVE TEST
  test("get all customer data success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .get("/customer?page=1&row=3")
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("success");
    expect(result.length).toBeGreaterThan(0);
  });

  test("get customer by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app).get("/customer/1").set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("get customer by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .get("/customer/999")
      .set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});

describe("customerController put function", () => {
  // POSITIVE TEST
  test("update data customer success", async () => {
    const createData = await Customer.create({
      name: "Bambang",
      age: 19,
      phone_number: "0892123213",
    });
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      name: "Bambang Rajawali",
      age: 19,
      phone_number: "0892123213",
    };
    const response = await request(app)
      .put(`/customer/${createData.id}`)
      .set(userValidation)
      .send(data);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  // NEGATIVE TEST
  test("update data customer failed because id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      name: "Bambang Rajawali",
      age: 19,
      phone_number: "0892123213",
    };
    const response = await request(app)
      .put(`/customer/999`)
      .set(userValidation)
      .send(data);

    const { status, result } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
  });
});

describe("customerController delete function", () => {
  // POSITIVE TEST

  test("delete customer by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const createdCustomer = await Customer.create({
      name: "Ahmad",
      age: 23,
      phone_number: "08127321723",
    });
    const response = await request(app)
      .delete(`/customer/${createdCustomer.id}`)
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("delete customer by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .delete("/customer/999")
      .set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});
