const { Event } = require("../models");

const getAllEvent = async (req, res) => {
  try {
    let { page, row } = req.query;

    page -= 1;

    const options = {
      attributes: {
        exclude: ["createdAt", "updatedAt"],
      },
      offset: page * row,
      limit: row,
    };
    const allEvent = await Event.findAll(options);
    if (allEvent.length <= 0) {
      return res.status(404).json({
        status: "failed",
        message: "Empty Data",
      });
    }
    return res.status(200).json({
      status: "success",
      result: allEvent,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const getEventById = async (req, res) => {
  try {
    const foundEvent = await Event.findByPk(req.params.id);

    if (!foundEvent) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(200).json({
      status: "Success",
      data: foundEvent,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const createEvent = async (req, res) => {
  try {
    const { event_name, date } = req.body;

    const createdEvent = await Event.create({
      event_name,
      date,
    });
    return res.status(201).json({
      status: "Success",
      data: createdEvent,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const updateEventById = async (req, res) => {
  const eventId = req.params.id;
  try {
    const { event_name, date } = req.body;
    const updateEvent = await Event.update(
      { event_name, date },
      {
        where: {
          id: eventId,
        },
      }
    );
    const options = {
      attributes: {
        exclude: ["createdAt", "updatedAt"],
      },
    };
    const foundEventUpdated = await Event.findByPk(eventId, options);
    if (!foundEventUpdated) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      data: foundEventUpdated,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const deleteEventById = async (req, res) => {
  const eventId = req.params.id;
  try {
    const deleteEvent = await Event.destroy({
      where: {
        id: eventId,
      },
    });
    if (deleteEvent == 0) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      msg: "Data berhasil di delete",
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

module.exports = {
  getAllEvent,
  getEventById,
  createEvent,
  updateEventById,
  deleteEventById,
};
