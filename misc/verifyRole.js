const jwt = require("jsonwebtoken");
const { User, Role } = require("../models");
const emailTransporter = require("./mailer");
const { EMAIL_BUSINESS } = process.env;

const verifyToken = (req, res, next) => {
  const authHeader = req.headers.token;
  if (authHeader) {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
      if (err) return res.status(401).json("Token is not valid!");
      req.user = user; //THISSSSSS
      next();
    });
  } else {
    return res.status(401).json("You are not authenticated!");
  }
};

const verifyTokenForRegistration = async (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
      if (err) return res.status(401).json("Token is not valid!");
      req.user = user; //THISSSSSS
      next();
    });
  } else {
    try {
      const { username, email, password } = req.body;
      const foundUser = await User.findOne({
        where: {
          email: email,
        },
      });
      if (!foundUser) {
        const createdUser = await User.create({
          username: username,
          email: email,
          password: password,
        });
        const from = EMAIL_BUSINESS;
        const htmlMessage = `<h2> Welcome to Application ${username}</h2>`;
        const subject = `Welcome Message`;
        //send welcome email
        const emailResponse = await emailTransporter(
          from,
          email,
          subject,
          htmlMessage
        );
        return res.status(200).json({
          status: "Success",
          message: "user create with role member",
        });
      }
      //message error for email already in use in application
      return res.status(400).json({
        status: "Failed",
        message: "Email Already In Use",
      });
    } catch (error) {
      return res.status(503).json({
        status: "Failed",
        message: "System ERROR",
      });
    }
  }
};

const verifyTokenAdminAndSA = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.role_id === 2 || req.user.role_id === 3) {
      next();
    } else {
      res.status(403).json("you are not allowed");
    }
  });
};

const verifyTokenAndMember = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.role_id === 1) {
      next();
    } else {
      res.status(403).json("you are not allowed");
    }
  });
};

const verifyTokenAndAdmin = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.role_id === 2) {
      next();
    } else {
      res.status(403).json("you are not allowed");
    }
  });
};

const verifyTokenAndSuperAdmin = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.role_id === 3) {
      next();
    } else {
      res.status(403).json("you are not allowed");
    }
  });
};

module.exports = {
  verifyToken,
  verifyTokenForRegistration,
  verifyTokenAdminAndSA,
  verifyTokenAndMember,
  verifyTokenAndAdmin,
  verifyTokenAndSuperAdmin,
};
