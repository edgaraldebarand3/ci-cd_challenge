const app = require("../app");

const { sequelize, Event } = require("../models");

const request = require("supertest");

const eventData = require("../masterdata/event.json").map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  return eachData;
});

beforeAll((done) => {
  sequelize.queryInterface
    .bulkInsert("Events", eventData, {})
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

afterAll((done) => {
  sequelize.queryInterface
    .bulkDelete("Events", null, {
      truncate: true,
      restartIdentity: true,
    })
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

describe("eventsController get function", () => {
  // POSITIVE TEST
  test("get all event data success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .get("/event?page=1&row=3")
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("success");
    expect(result.length).toBeGreaterThan(0);
  });

  test("get event by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app).get("/event/1").set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(200);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("get event by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app).get("/event/999").set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});

describe("eventController put function", () => {
  // POSITIVE TEST
  test("update data event success", async () => {
    const createData = await Event.create({
      event_name: "Synchronize Fest",
      date: "2022-04-21 20:00",
    });
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      event_name: "Synchronize Fest 2022",
      date: "2022-12-12 20:00",
    };
    const response = await request(app)
      .put(`/event/${createData.id}`)
      .set(userValidation)
      .send(data);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  // NEGATIVE TEST
  test("update data event failed because id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const data = {
      event_name: "Synchronize Fest 2022",
      date: "2022-12-12 20:00",
    };
    const response = await request(app)
      .put(`/event/999`)
      .set(userValidation)
      .send(data);

    const { status, result } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
  });
});

describe("eventController delete function", () => {
  // POSITIVE TEST

  test("delete event by Id Success", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const createData = await Event.create({
      event_name: "Synchronize Fest",
      date: "2022-04-21 20:00",
    });
    const response = await request(app)
      .delete(`/event/${createData.id}`)
      .set(userValidation);
    const { status, result } = response.body;
    expect(response.status).toBe(201);
    expect(status).toBe("Success");
  });

  //NEGATIVE TEST
  test("delete event by Id failed -> id not found", async () => {
    const userValidation = { username: "edgar", password: "12345" };
    const response = await request(app)
      .delete("/event/999")
      .set(userValidation);
    const { status, message } = response.body;
    expect(response.status).toBe(404);
    expect(status).toBe("Failed");
    expect(message).toBe("Id Not Found");
  });
});
