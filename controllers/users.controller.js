const { User, Role } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { verifyTokenForRegistration } = require("../misc/verifyRole");
const emailTransporter = require("../misc/mailer");
const { SALT_ROUNDS, EMAIL_BUSINESS } = process.env;

const userController = {
  login: async (req, res) => {
    const { email, password } = req.body;
    const foundUser = await User.findOne({
      where: {
        email: email,
      },
    });
    const isValidPassword = bcrypt.compareSync(password, foundUser.password);
    if (isValidPassword) {
      const payload = {
        id: foundUser.id,
        username: foundUser.username,
        email: foundUser.email,
        role_id: foundUser.role_id,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: "1h",
      });
      return res.status(200).json({
        token: token,
      });
    }
    return res.status(400).json({
      status: "Failed",
      message: "Wrong email or password",
    });
  },
  register: async (req, res) => {
    verifyTokenForRegistration(req, res, async () => {
      const { username, email, password, role_id } = req.body;
      try {
        const findRole = await Role.findByPk(role_id);
        const foundUser = await User.findOne({
          where: {
            email: email,
          },
        });
        if (!foundUser) {
          switch (req.user.role_id) {
            case 1:
              const createdUserMember = await User.create({
                username: username,
                email: email,
                password: password,
              });
              return res.status(200).json({
                status: "Success",
                message: `user create with role member`,
              });
            case 2:
              if (role_id == 3) {
                return res.status(404).json({
                  status: "Failed",
                  message: "You Are Not Allowed Create This Role",
                });
              }
              const createdUserAdmin = await User.create({
                username: username,
                email: email,
                password: password,
                role_id: role_id,
              });

              return res.status(200).json({
                status: "Success",
                message: `user create with role ${findRole.name}`,
              });
            case 3:
              const createdUserSuperAdmin = await User.create({
                username: username,
                email: email,
                password: password,
                role_id: role_id,
              });

              return res.status(200).json({
                status: "Success",
                message: `user create with role ${findRole.name}`,
              });
            default:
              return res.status(404).json({
                status: "failed",
                message: "Error Role Id Not Found",
              });
          }
        }
        return res.status(400).json({
          status: "Failed",
          message: "Email Already In Use",
        });
      } catch (error) {
        return res.status(503).json({
          status: "Failed",
          message: "Error with System",
        });
      }
    });
  },
  getAll: async (req, res) => {
    try {
      let { page, row } = req.query;
      page -= 1;

      const options = {
        include: [
          {
            model: Role,
            attributes: ["name"],
          },
        ],
        attributes: {
          exclude: ["createdAt", "updatedAt", "password"],
        },
        // offset: page * row,
        // limit: row,
        order: [["id", "ASC"]],
      };

      const allUser = await User.findAll(options);
      if (allUser.length > 0) {
        return res.status(200).json({
          status: "Sucess",
          data: allUser,
        });
      }
      return res.status(400).json({
        status: "Failed",
        message: "Empty Data",
      });
    } catch (error) {
      return res.status(503).json({
        message: "System Error",
      });
    }
  },
  create: async (req, res) => {
    try {
      const { username, email, password, role_id, profile_picture_url } =
        req.body;
      const createdUser = await User.create({
        username: username,
        email: email,
        password: password,
        role_id: role_id,
        profile_picture: profile_picture_url,
      });
      const options = {
        include: [
          {
            model: Role,
            attributes: ["name"],
          },
        ],
        attributes: {
          exclude: ["createdAt", "updatedAt", "password"],
        },
      };
      if (createdUser) {
        const foundUser = await User.findByPk(createdUser.id, options);

        return res.status(200).json({
          status: "Sucess",
          data: foundUser,
        });
      }

      return res.status(400).json({
        status: "Failed",
        message: "User Not Create",
      });
    } catch (error) {
      return res.status(503).json({
        status: "Failed",
        message: "System Error",
      });
    }
  },

  update: async (req, res) => {
    const userId = req.params.id;
    try {
      const updatedUser = await User.update(req.body, {
        where: {
          id: userId,
        },
      });

      if (updatedUser) {
        const options = {
          include: [
            {
              model: Role,
              attributes: ["name"],
            },
          ],
          attributes: {
            exclude: ["createdAt", "updatedAt", "password"],
          },
        };
        const foundUser = await User.findByPk(userId, options);

        return res.status(200).json({
          status: "Sucess",
          data: foundUser,
        });
      }
      return res.status(400).json({
        status: "Failed",
        message: "User Id Not Found",
      });
    } catch (error) {
      return res.status(503).json({
        status: "Failed",
        message: "System Error",
      });
    }
  },
  delete: async (req, res) => {
    userId = req.params.id;
    try {
      const deleteUser = await User.destroy({
        where: {
          id: userId,
        },
      });

      if (deleteUser) {
        return res.status(200).json({
          status: "Success",
          message: "User Deleted",
        });
      }

      return res.status(403).json({
        status: "Failed",
        message: "Id Not Found",
      });
    } catch (error) {
      return res.status(503).json({
        status: "Failed",
        message: "System Error",
      });
    }
  },

  getOtp: async (req, res) => {
    try {
      // Get email from body request
      const { email } = req.body;

      // Create unique OTP
      const otp = Math.random().toString(36).slice(-6).toUpperCase();

      // Hash generated OTP
      const hashedOtp = bcrypt.hashSync(otp, +SALT_ROUNDS);

      const updatedUser = await User.update(
        {
          otp: hashedOtp,
        },
        {
          where: {
            email: email,
          },
        }
      );

      if (!updatedUser[0])
        throw {
          message: "Email is not registered",
          status: "Failed",
          code: 404,
        };
      const fromEmail = EMAIL_BUSINESS;
      const otpMessage = `<h2>Your OTP is ${otp}, please use this otp as your credential to retrieve the password<h2>`;
      const subject = "Your OTP Verification";
      const emailResponse = await emailTransporter(
        fromEmail,
        email,
        subject,
        otpMessage
      );

      return res.status(200).json({
        message: `OTP successfully sent to ${emailResponse.accepted
          .join(",")
          .split(",")}`,
      });
    } catch (error) {
      if (error.code) {
        return res.status(error.code).json({
          status: error.status,
          message: error.message,
        });
      }
      next(err);
    }
  },
  forgetPassword: async (req, res) => {
    try {
      // Retrieve information from body request
      const { email, newPassword, newPasswordConfirmation, otp } = req.body;

      if (newPassword === newPasswordConfirmation) {
        // Search User
        const foundUser = await User.findOne({
          where: {
            email: email,
          },
        });

        // Check if user exists
        if (!foundUser)
          throw {
            message: "Email is not registered",
            status: "Failed",
            code: 404,
          };

        // If User exists, check OTP
        const compareOtp = bcrypt.compareSync(otp, foundUser.otp);

        if (compareOtp) {
          await foundUser.update({
            password: bcrypt.hashSync(newPassword, +SALT_ROUNDS),
            otp: null,
          });

          return res.status(200).json({
            status: "Success",
            message: "Password successfully changed",
          });
        }

        throw { message: "Wrong OTP", status: "Failed", code: 400 };
      }

      throw { message: `Password doesn't match`, status: "Failed", code: 400 };
    } catch (error) {
      if (error.code) {
        return res.status(error.code).json({
          status: error.status,
          message: error.message,
        });
      }
      next(err);
    }
  },
  uploadVideo: async (req, res) => {
    const { url_video } = req.body;
    return res.status(201).json({
      status: "Succes",
      message: "Upload Video Sucess",
      Url_Video: url_video,
    });
  },
};

module.exports = userController;
