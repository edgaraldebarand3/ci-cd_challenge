const cloudinary = require("cloudinary").v2;
const fs = require("fs");
const { CLOUDINARY_NAME, CLOUDINARY_API_KEY, CLOUDINARY_API_SECRET } =
  process.env;
cloudinary.config({
  cloud_name: CLOUDINARY_NAME,
  api_key: CLOUDINARY_API_KEY,
  api_secret: CLOUDINARY_API_SECRET,
});

const uploadWithCloudinary = async (req, res, next) => {
  try {
    const foldering = `my-asset/${req.file.mimetype.split("/")[0]}`;
    const uploadResult = await cloudinary.uploader.upload(req.file.path, {
      folder: foldering,
    });
    fs.unlinkSync(req.file.path);
    req.body.profile_picture_url = uploadResult.secure_url;
    next();
  } catch (error) {
    fs.unlinkSync(req.file.path);
  }
};

const uploadVideoWithCloudinary = async (req, res, next) => {
  try {
    const foldering = `my-asset/${req.file.mimetype.split("/")[0]}`;
    const uploadResult = await cloudinary.uploader.upload(req.file.path, {
      resource_type: "video",
      folder: foldering,
    });
    fs.unlinkSync(req.file.path);
    req.body.url_video = uploadResult.secure_url;
    next();
  } catch (error) {
    fs.unlinkSync(req.file.path);
  }
};

module.exports = { uploadWithCloudinary, uploadVideoWithCloudinary };
