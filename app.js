require("dotenv").config();
const express = require("express");
const router = require("./routes/index.route");
const app = express();
const port = process.env.PORT;
const swaggerJSON = require("./api-documentation/swagger.json");
const swaggerUI = require("swagger-ui-express");
const morgan = require("morgan")("dev");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use("/static/cd-games", express.static("public"));

app.use(morgan);
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(router);

module.exports = app;
