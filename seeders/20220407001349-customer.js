"use strict";

const customerData = require("../masterdata/customer.json");

module.exports = {
  async up(queryInterface, Sequelize) {
    const customerDataWithoutId = customerData.map((eachCustomerData) => {
      delete eachCustomerData.id;

      eachCustomerData.createdAt = new Date();
      eachCustomerData.updatedAt = new Date();
      return eachCustomerData;
    });
    await queryInterface.bulkInsert("Customers", customerDataWithoutId);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Customers", null);
  },
};
