const express = require("express");
const router = express.Router();

const {
  getAllEvent,
  getEventById,
  createEvent,
  updateEventById,
  deleteEventById,
} = require("../controllers/events.controller");
const { userValidation } = require("../misc/middleware");

router.get("/", userValidation, getAllEvent);
router.get("/:id", userValidation, getEventById);
router.post("/", userValidation, createEvent);
router.put("/:id", userValidation, updateEventById);
router.delete("/:id", userValidation, deleteEventById);

module.exports = router;
