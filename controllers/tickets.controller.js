const { Ticket } = require("../models");
const ticket = require("../models/ticket");
const { Customer, Event } = require("../models");

const getAllTicket = async (req, res) => {
  try {
    let { page, row } = req.query;

    page -= 1;

    const options = {
      include: [
        {
          model: Customer,
          attributes: ["name", "phone_number"],
        },
        {
          model: Event,
          attributes: ["event_name"],
        },
      ],
      attributes: ["id", "ticket_name", "price", "customer_id", "event_id"],
      offset: page * row,
      limit: row,
    };

    const allTicket = await Ticket.findAll(options);
    if (allTicket.length <= 0) {
      return res.status(404).json({
        status: Failed,
        message: "Empty Data",
      });
    }
    return res.status(200).json({
      status: "success",
      result: allTicket,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const getTicketById = async (req, res) => {
  try {
    const foundTicket = await Ticket.findByPk(req.params.id);

    if (!foundTicket) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(200).json({
      status: "Success",
      data: foundTicket,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const createTicket = async (req, res) => {
  try {
    const { ticket_name, price, customer_id, event_id } = req.body;

    const createdTicket = await Ticket.create({
      ticket_name: ticket_name,
      price: price,
      customer_id: customer_id,
      event_id: event_id,
    });
    return res.status(201).json({
      status: "Success",
      data: createdTicket,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const updateTicketById = async (req, res) => {
  const ticketId = req.params.id;
  try {
    const { ticket_name, price, customer_id, event_id } = req.body;
    const updateTicket = await Ticket.update(
      {
        ticket_name: ticket_name,
        price: price,
        customer_id: customer_id,
        event_id: event_id,
      },
      {
        where: {
          id: ticketId,
        },
      }
    );
    const foundTicketUpdated = await Ticket.findByPk(ticketId);
    if (!foundTicketUpdated) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      data: req.body,
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

const deleteTicketById = async (req, res) => {
  const ticketId = req.params.id;
  try {
    const deleteTicket = await Ticket.destroy({
      where: {
        id: ticketId,
      },
    });
    if (deleteTicket == 0) {
      return res.status(404).json({
        status: "Failed",
        message: "Id Not Found",
      });
    }
    return res.status(201).json({
      status: "Success",
      msg: "Data berhasil di delete",
    });
  } catch (error) {
    return res.status(503).json({ status: "Failed", message: "System Error" });
  }
};

module.exports = {
  getAllTicket,
  getTicketById,
  createTicket,
  updateTicketById,
  deleteTicketById,
};
