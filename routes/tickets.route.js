const express = require("express");
const router = express.Router();
const {
  getAllTicket,
  getTicketById,
  createTicket,
  updateTicketById,
  deleteTicketById,
} = require("../controllers/tickets.controller");
const { userValidation } = require("../misc/middleware");
const ticketValidator = require("../validation/ticket.validation");
const validate = require("../validation/validate");

router.get("/", userValidation, getAllTicket);
router.get("/:id", userValidation, getTicketById);
router.post(
  "/",
  userValidation,
  ticketValidator.create(),
  validate,
  createTicket
);
router.put(
  "/:id",
  userValidation,
  ticketValidator.update(),
  validate,
  updateTicketById
);
router.delete("/:id", userValidation, deleteTicketById);

module.exports = router;
