const { body } = require("express-validator");
const { Role } = require("../models");

module.exports = {
  create: () => [
    body("role_id").custom((value) => {
      return Role.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Role with id ${value} is not found`);
      });
    }),
  ],
  update: () => [
    body("role_id").custom((value) => {
      return Role.findOne({
        where: {
          id: value,
        },
      }).then((user) => {
        if (!user) throw new Error(`Role with id ${value} is not found`);
      });
    }),
  ],
};
