const express = require("express");
const router = express.Router();
// const { logger, sanitation } = require("../misc/middleware");
const customerRoutes = require("./customers.route");
const ticketRoutes = require("./tickets.route");
const eventRoutes = require("./event.route");
const userRoutes = require("./users.route");

router.use("/customer", customerRoutes);
router.use("/ticket", ticketRoutes);
router.use("/event", eventRoutes);
router.use("/user", userRoutes);

module.exports = router;
